import React, { Component } from 'react';
import {hotkeys} from 'react-keyboard-shortcuts'

class Humidity extends Component {

    constructor() {
        super();
        this.state = {
            value: 'FETCHING...'
        }
    }

    hot_keys = {
        'esc': {
          handler: (event) => this.props.history.push('/menu/plants')
        }
    }

    componentDidMount() {
        fetch('/sensor/humidity').then(result => {
            return result.json(); 
        }).then(data => {
            this.setState({value: data.value})
        })
    }

    render() {
        return (
            <span>
            CURRENT AIR HUMIDITY: {this.state.value}%<br /><br />
            </span>
        );
    }
}

export default hotkeys(Humidity);