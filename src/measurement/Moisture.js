import React, { Component } from 'react';
import {hotkeys} from 'react-keyboard-shortcuts'

class Moisture extends Component {

    constructor() {
        super();
        this.state = {
            value: 'FETCHING...'
        }
    }

    hot_keys = {
        'esc': {
          handler: (event) => this.props.history.push('/menu/plants')
        }
    }

    componentDidMount() {
        fetch('/sensor/moisture').then(result => {
            return result.json(); 
        }).then(data => {
            this.setState({value: data.value})
        })
    }

    render() {
        return (
            <span>
            CURRENT SOIL MOISTURE: {this.state.value}%<br /><br />
            </span>
        );
    }
}

export default hotkeys(Moisture);