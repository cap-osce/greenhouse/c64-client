import React, { Component } from 'react';
import {hotkeys} from 'react-keyboard-shortcuts'

class Light extends Component {

    constructor() {
        super();
        this.state = {
            value: 'FETCHING...'
        }
    }

    hot_keys = {
        'esc': {
          handler: (event) => this.props.history.push('/menu/plants')
        }
    }

    componentDidMount() {
        fetch('/sensor/light').then(result => {
            return result.json(); 
        }).then(data => {
            this.setState({value: data.value})
        })
    }

    render() {
        return (
            <span>
            CURRENT LIGHT LEVEL: {this.state.value}%<br /><br />
            </span>
        );
    }
}

export default hotkeys(Light);