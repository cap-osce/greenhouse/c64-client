import React, { Component } from 'react';
import {hotkeys} from 'react-keyboard-shortcuts'

class Temperature extends Component {

    constructor() {
        super();
        this.state = {
            value: 'FETCHING...'
        }
    }

    hot_keys = {
        'esc': {
          handler: (event) => this.props.history.push('/menu/plants')
        }
    }

    componentDidMount() {
        fetch('/sensor/temperature').then(result => {
            return result.json(); 
        }).then(data => {
            this.setState({value: data.value})
        })
    }

    render() {
        return (
            <span>
            CURRENT TEMPERATURE: {this.state.value}&#x259d;C<br /><br />
            </span>
        );
    }
}

export default hotkeys(Temperature);