import React, { Component } from 'react';
import './App.css';
import { HashRouter as Router, Route, Redirect } from "react-router-dom";
import Main from './menu/Main';
import Plants from './menu/Plants';
import Actions from './menu/Actions';
import Survey from './survey/Survey';
import Light from './measurement/Light';
import Temperature from './measurement/Temperature';
import Moisture from './measurement/Moisture';
import Humidity from './measurement/Humidity';

class App extends Component {
  render() {
    return (
      <Router>
        <div class="screen">
          <span class="text" id="text">
            <br />
            &nbsp;&nbsp;&nbsp;**** GREENHOUSE 64 PLANTS V2 ****<br /><br />
            &nbsp;64K RAM SYSTEM&nbsp;&nbsp;&nbsp;&nbsp;(C) 2019 CAPGEMINI &#x2660;<br /><br />
            ----------------------------------------<br /><br />
            <Route exact path="/" render={() => (
              <Redirect to="/menu/main"/>
            )}/>
            <Route path="/menu/main" component={Main} />
            <Route path="/menu/plants" component={Plants} />
            <Route path="/measurement/light" component={Light} />
            <Route path="/measurement/temperature" component={Temperature} />
            <Route path="/measurement/humidity" component={Humidity} />
            <Route path="/measurement/moisture" component={Moisture} />
            <Route path="/menu/actions" component={Actions} />              
            <Route path="/survey" component={Survey} />
          </span>
          <span class="cursor">&nbsp;</span>
        </div>
      </Router>
    );
  }
}

export default App;
