import React, { Component } from 'react';
import {hotkeys} from 'react-keyboard-shortcuts'

class Survey extends Component {

    hot_keys = {
        'esc': {
          handler: (event) => this.props.history.push('/menu/main')
        }
    }

    render() {
        return (
            <span>
            PLEASE SEE A MEMBER OF THE CAPGEMINI<br /><br />
            STAFF TO FILL IN OUR SURVEY<br /><br />
            </span>
        );
    }
}

export default hotkeys(Survey);