import React, { Component } from 'react';
import {hotkeys} from 'react-keyboard-shortcuts'

class Plants extends Component {

    hot_keys = {
        'esc': {
          handler: (event) => this.props.history.push('/menu/main')
        },
        '1': {
            handler: (event) => this.props.history.push('/measurement/light')
        },
        '2': {
            handler: (event) => this.props.history.push('/measurement/temperature')
        },
        '3': {
            handler: (event) => this.props.history.push('/measurement/humidity')
        },
        '4': {
            handler: (event) => this.props.history.push('/measurement/moisture')
        }
    }

    render() {
        return (
            <span>
            PLEASE SELECT ONE OF THE FOLLOWING:<br /><br />
            1. LIGHT.<br /><br />
            2. TEMPERATURE.<br /><br />
            3. HUMIDITY.<br /><br />
            4. SOIL MOISTURE.<br /><br />
            </span>
        );
    }
}

export default hotkeys(Plants);