import React, { Component } from 'react';
import {hotkeys} from 'react-keyboard-shortcuts'

class Main extends Component {

    hot_keys = {
        '1': {
          handler: (event) => this.props.history.push('/menu/plants')
        },
        '2': {
          handler: (event) => this.props.history.push('/menu/actions')
        },
        '3': {
          handler: (event) => this.props.history.push('/survey')
        }
      }

    render() {
        return (
            <span>
            PLEASE SELECT ONE OF THE FOLLOWING:<br /><br />
            1. REQUEST PLANT INFORMATION.<br /><br />
            2. PERFORM A GREENHOUSE ACTION.<br /><br />
            3. TAKE A SURVEY FOR FREE BEER.<br /><br />
            </span>
        );
    }
}

export default hotkeys(Main);