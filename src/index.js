import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';

ReactDOM.render(<App />, document.getElementById('root'));

(function(global) {
    function scaleAmountNeededToFit(el, root) {
      const parentSize = {
        width: root.clientWidth,
        height: Math.max(root.clientHeight, window.innerHeight)
      };

      console.log({
          pEl: root,
          item: {
            w: el.clientWidth,
            h: el.clientHeight
          }, 
          p: {
              w: root.clientWidth,
              h: root.clientHeight
          }
      })

      return Math.min(parentSize.width / el.clientWidth, parentSize.height / el.clientHeight);
    }
  
    function fitToParent(element) {
        var root = element;
        while (root.nodeName !== 'BODY') {
            root = root.parentElement;
        }

      const scale = scaleAmountNeededToFit(element, root);
      element.style.transformOrigin = "0 0";
      element.style.transform = `scale(${scale})`;
 
      var parentWidth = root.clientWidth;
      var itemWidth = element.clientWidth * scale;
      var translation = (parentWidth - itemWidth)/2;
      console.log({
          screen: parentWidth,
          block: itemWidth,
          diff: translation,
          scale: scale
      });
      element.style.transform = `translate(${translation}px,0px) scale(${scale})`;
    }
  
    global.fitToParent = fitToParent;
  })(window);